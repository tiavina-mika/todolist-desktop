import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';
import EditIcon from '@material-ui/icons/Edit';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import Tooltip from '@material-ui/core/Tooltip';

import DialogDetail from './dialog-detail';
import { blue } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  paper: {
    padding: theme.spacing(3),
    minWidth: 500
  },
  paperForm: {
        display: 'flex',
        alignItems: 'flex-end',
        marginBottom: theme.spacing(2)
  },
  listItem: {
    // backgroundColor: theme.palette.background.paper,
    borderBottom: '1px solid rgba(0, 0, 0, 0.2)'
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
  checkIcon: {
    color: blue[500],
    '&:hover': {
      color: blue[700],
    },
  }
}));


const UserList = props => {
    const { users } = props;

    const classes = useStyles();
    const [selected, setSelected] = useState({user: null, open: false, type: null})

    const handleOpenEdit = (user) => {
      setSelected({user, open: !selected.open, type: 'edit'})
    }
    const handleOpenDetail = (user) => {
      setSelected({user, open: !selected.open, type: 'detail'})
    }
    
    const handleClose = () => {
      setSelected({user: null, open: false, type: null})
    }

    const filterUser = selected.user && users.filter(t => t.id === selected.user.id);
    const filterUserId = filterUser && filterUser[0].id;
    return (
      <>
        { users.map(user => (
            <ListItem className={classes.listItem} key={user.id}>
                <ListItemAvatar>
                    <Avatar style={{backgroundColor: 'transparent'}}>
                      { user.confirmed
                          ? <RadioButtonCheckedIcon color="primary" className={classes.checkIcon}/>
                          : <RadioButtonUncheckedIcon color="primary" className={classes.checkIcon}/>
                      }
                    </Avatar>
                </ListItemAvatar>
                <ListItemText
                    primary={user.username}
                    secondary={user.email}
                />
                <ListItemSecondaryAction>
                    <Tooltip title="User Detail">
                        <IconButton edge="end" aria-label="detail" onClick={()=> handleOpenDetail(user)}>
                            <ControlPointIcon />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Edit User">
                        <IconButton edge="end" aria-label="edit" onClick={()=> handleOpenEdit(user)}>
                            <EditIcon />
                        </IconButton>
                    </Tooltip>
                </ListItemSecondaryAction>
            </ListItem>
        ))}
            { selected.type === "detail" && selected.user && selected.user.id === filterUserId
              && <DialogDetail
                    open={selected.open}
                    onClose={handleClose}
                    user={selected.user}
                  />
          }
      </>
  );
}

export default UserList;