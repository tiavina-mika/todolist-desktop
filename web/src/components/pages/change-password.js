import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import ChangePasswordForm from '../forms/change-password-form';
import { makeStyles } from '@material-ui/core/styles';
import { useParams } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  container: {
      paddingTop: theme.spacing(15)
  },
  main: {
      backgroundColor: theme.palette.common.white,
  },
}));

const ChangePassword = () => {
  const classes = useStyles();
  let { id } = useParams();
  let text = "Change password";

  if (id) {
    text = "New Password";
  }

  return (
    <Container component="main" maxWidth="xs" className={classes.container}>
      <CssBaseline />
      <Box display="flex" flexDirection="column" alignItems="center" pl={5} pt={3}  pr={5} pb={5} className={classes.main}>
        <Typography component="h1" variant="h5">
          { text }
        </Typography>
        <ChangePasswordForm id={id}/>
      </Box>
    </Container>
  );
}

export default ChangePassword;