import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import useUsersQuery from '../../graphql/queries/user/users';

import Pagination from '../blocks/pagination';
import SelectSort from '../forms/select-sort';
import SelectStatus from '../forms/select-status';
import SelectLimit from '../forms/select-limit';
import UserList from '../users/list';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  paper: {
    padding: theme.spacing(3),
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
  },
  paperCenter: {
    minWidth: 500,
  },
  paperRight: {
    minWidth: 300,
    height: 370
  },
  paperLeft: {
    minWidth: 300,
    height: 360
  },
  paperForm: {
        display: 'flex',
        alignItems: 'flex-end',
        marginBottom: theme.spacing(2)
  },
  listItem: {
    borderBottom: '1px solid rgba(0, 0, 0, 0.2)'
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
}));

const Home = () => {
    const classes = useStyles();
    const [page, setPage] = useState(0);
    const [sort, setSort] = useState('CREATEDAT');
    const [confirmed, setConfirmed] = useState(null);
    const [limit, setLimit] = useState(10);
    const [currentUser, setCurrentUser] = useState(null);
    const { loading, data } = useUsersQuery(sort, limit, page, confirmed);

    useEffect(() => {
        data && setPage(data.users.currentPage);
    }, [data])

    const handleChangePage = page => {
        setPage(page)
    }

    const handleSelectSort = sort => {
        setSort(sort);
    }
    const handleSelectConfirmed = confirmed => {
      setConfirmed(confirmed);
    }
    const handleLimit = limit => {
        setLimit(parseInt(limit));
    }

    const handleCurrentUser = user => {
        setCurrentUser(user);
    }

    if (loading) return <Box display="flex" justifyContent="center"><CircularProgress/></Box>;
    return (
            <Box display="flex" alignItems="flex-start" justifyContent="center" height="100vh" pt={15}>
                <Paper className={clsx(classes.paper, classes.paperCenter)}>
                  { data && data.users.users.length > 0
                    ? <List>
                        <UserList
                            users={data.users.users}
                            variables={{sort, limit, page, confirmed: typeof(confirmed) === 'boolean' ? confirmed: null}}
                            onEdit={handleCurrentUser}
                        />
                        { data.users.pages < data.users.total
                          && <Pagination
                                currentPage={page}
                                total={data.users.total}
                                onChangePage={handleChangePage}
                                limit={limit}
                              />
                        }
                    </List>
                    : <Box display="flex" justifyContent="center" alignItems="center" height="30vh">
                        <Typography variant="h6">No User Found</Typography>
                    </Box>          
                  }
                </Paper>

                <Paper className={clsx(classes.paper, classes.paperRight)}>
                    <Box>
                        <SelectSort onSelect={handleSelectSort} sort={sort} type="user"/>
                        <SelectStatus onSelect={handleSelectConfirmed} confirmed={confirmed}/>
                        { data && data.users.users.length > 1 &&
                          <>
                            <SelectLimit onSelect={handleLimit} limit={limit}/>
                          </>
                        }
                    </Box>
                </Paper>
            </Box>
  );
}

export default Home;