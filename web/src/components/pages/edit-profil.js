import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import EditProfilForm from '../forms/edit-profil-form';
import useProfilQuery from '../../graphql/mutations/auth/profil';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    container: {
        paddingTop: theme.spacing(15)
    },
    main: {
        backgroundColor: theme.palette.common.white,
    },
}));

const EditProfil = () => {
    const { loading, data } = useProfilQuery();
    const classes = useStyles();

    if (loading) return <Box display="flex" justifyContent="center"><CircularProgress/></Box>;

    return (
        <Container component="main" maxWidth="sm" className={classes.container}>
            <CssBaseline />
            <Box display="flex" flexDirection="column" alignItems="center" pl={5} pt={3}  pr={5} pb={5} className={classes.main}>
                <Typography component="h1" variant="h5">
                    Edit Profil
                </Typography>
                <EditProfilForm currentUser={data.profil}/>
            </Box>
        </Container>
    );
}

export default EditProfil;