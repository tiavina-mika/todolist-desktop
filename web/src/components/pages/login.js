import React from 'react';
import { Link } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import LoginForm from '../forms/login-form';
import { blue } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  error: {
    color: '#9e1c1c',
    marginTop: theme.spacing(3)
  },
  link: {
    textDecoration: 'none',
    color: blue[500],
    '&:hover': {
        color: blue[700],
    },
  }
}));

const Login = () => {
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box display="flex" flexDirection="column" alignItems="center" mt={{ xs: 1, sm: 1, md: 8 }}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <LoginForm />
      </Box>
      <Box m={1}>
        <Typography><Link to="/register" className={classes.link}> Create an account</Link></Typography>
      </Box>
      <Box m={1}>
        <Typography><Link to="/check-email" className={classes.link}>Forgotten password?</Link></Typography>
      </Box>
    </Container>
  );
}

export default Login;