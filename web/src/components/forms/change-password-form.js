import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router";
import { blue } from '@material-ui/core/colors';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import useChangePasswordMutation from '../../graphql/mutations/auth/change-password';
import useForgottenPasswordMutation from '../../graphql/mutations/auth/forgotten-password';
import { PASSWORD_LENGTH,  PASSWORD_EMPTY, CONFIRM_PASSWORD_NOT_MATCH } from '../../utils/constant';

const useStyles = makeStyles(theme => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    marginTop: theme.spacing(1),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    backgroundColor: blue[500],
    '&:hover': {
        backgroundColor: blue[700],
    },
  },
}));

const ChangePasswordForm = ({ id }) => {
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const history = useHistory();

    const [errorMessages, setErrorMessages] = useState(null);

    const handleError = ({ graphQLErrors }) => {
      if (graphQLErrors) {
          const error = graphQLErrors[0].message;
          const errorMessage = {
            password: '',
            confirmPassword: ''

          }
          error.split(',').map(e => {
              if (e === "PASSWORD_EMPTY") {
                errorMessage.password = PASSWORD_EMPTY;
              } else if (e === "PASSWORD_LENGTH") {
                errorMessage.password = PASSWORD_LENGTH;
              }
          })
          setErrorMessages(errorMessage);
          return errorMessage;
        }
    }

    let [changePassword, { loading }] = useChangePasswordMutation(handleError);
    let [ forgottenPassword ] = useForgottenPasswordMutation(handleError);

    const classes = useStyles();

    const submit = async (e) => {
        e.preventDefault();
        if (confirmPassword !== password) {
          setErrorMessages({confirmPassword: CONFIRM_PASSWORD_NOT_MATCH});
        }
        if (id) {
          const forgottenPassowrdResult = await forgottenPassword({ id, password });
          if (forgottenPassowrdResult && confirmPassword === password) {   
            setConfirmPassword('');
            setPassword('');
            history.push('/login');
          }
          return forgottenPassowrdResult;
        }

        const result = await changePassword({ password });

        if (result && confirmPassword === password) {
            setConfirmPassword('');
            setPassword('');
            history.push('/profil')
            return result;
        }
    }

    const handleChange = event => {
        const { name, value } = event.target;
        if (name === 'password') setPassword(value);
        if (name === 'confirmPassword') setConfirmPassword(value);
    }

    return (
        <form className={classes.form} onSubmit={submit} noValidate>
                <Box mt={2}>
                    <InputLabel className={classes.label}>Password</InputLabel>
                    <TextField
                        error={errorMessages && errorMessages.password ? true: false}
                        id="outlined-bare-password"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        name="password"
                        type="password"
                        onChange={handleChange}
                        inputProps={{ 'aria-label': 'bare' }}
                        fullWidth
                        value={password}
                        helperText={ errorMessages && errorMessages.password }
                    />
                </Box>
                <Box mt={2}>
                    <InputLabel className={classes.label}>Confirm Password</InputLabel>
                    <TextField
                        error={errorMessages && errorMessages.confirmPassword ? true: false}
                        id="outlined-bare-confirm"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        name="confirmPassword"
                        type="password"
                        onChange={handleChange}
                        inputProps={{ 'aria-label': 'bare' }}
                        fullWidth
                        value={confirmPassword}
                        helperText={ errorMessages && errorMessages.confirmPassword }
                    />
                </Box>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={loading}
          >
            Save
          </Button>
        </form>
  );
}

export default ChangePasswordForm;