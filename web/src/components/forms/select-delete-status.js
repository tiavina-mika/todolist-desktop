import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import CustomInput from './custom-input';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing(1),
  },
  primary: {
    color: '#fff'
  }
}));

const SelectDeleteStatus = props => {
  const { onSelect, deleteFilter, dark } = props;
  const classes = useStyles();
  const handleChange = event => {
      onSelect(event.target.value);
  };
  return (
    <form className={classes.root} autoComplete="off">
      <FormControl className={classes.margin}>
        <InputLabel htmlFor="select-delete-status" className={dark ? classes.primary: ''}>Delete</InputLabel>
        <Select
          value={deleteFilter}
          onChange={handleChange}
          input={<CustomInput name="delete" id="select-delete-status" dark={dark}/>}
        >
            <MenuItem value={false}>
                <em>None</em>
            </MenuItem>
            <MenuItem value={true}>Done</MenuItem>
            <MenuItem value={false}>Not Done</MenuItem>
            <MenuItem value={'none'}>All</MenuItem>
        </Select>
      </FormControl>
    </form>
  );
}

export default SelectDeleteStatus;