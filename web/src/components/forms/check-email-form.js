import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router";
import { blue } from '@material-ui/core/colors';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import useCheckEmailMutation from '../../graphql/mutations/auth/check-email';
import { EMAIL_INVALID, EMAIL_EMPTY } from '../../utils/constant';

const useStyles = makeStyles(theme => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    marginTop: theme.spacing(1),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    backgroundColor: blue[500],
    '&:hover': {
        backgroundColor: blue[700],
    },
  },
}));

const CheckEmailForm = () => {
    const [email, setEmail] = useState('');

    const [errorMessages, setErrorMessages] = useState(null);

    const handleError = ({ graphQLErrors }) => {
      if (graphQLErrors) {
          const error = graphQLErrors[0].message;
          const errorMessage = {
            email: '',
          }
          error.split(',').map(e => {
              if (e === "EMAIL_EMPTY") {
                errorMessage.email = EMAIL_EMPTY;
              } else if (e === "EMAIL_INVALID") {
                errorMessage.email = EMAIL_INVALID;
              }
          })
          setErrorMessages(errorMessage);
          return errorMessage;
        }
    }

    let [checkEmail, { loading }] = useCheckEmailMutation(handleError);

    let history = useHistory();
    const classes = useStyles();

    const submit = async (e) => {
        e.preventDefault();
        const result = await checkEmail({ email });

        if (result) {
          setEmail('');
          history.push(`/new-password/${result.data.checkEmail.id}`)
        }

        return result;
    }

    const handleChange = event => {
        const { name, value } = event.target;
        if (name === 'email') setEmail(value);
    }

    return (
        <form className={classes.form} onSubmit={submit} noValidate>
                <Box mt={2}>
                    <InputLabel className={classes.label}>Email</InputLabel>
                    <TextField
                        error={errorMessages && errorMessages.email ? true: false}
                        id="outlined-bare-email"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        name="email"
                        type="email"
                        onChange={handleChange}
                        inputProps={{ 'aria-label': 'bare' }}
                        fullWidth
                        value={email}
                        helperText={ errorMessages && errorMessages.email }
                    />
                </Box>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={loading}
          >
            Confirm
          </Button>
        </form>
  );
}

export default CheckEmailForm;