import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import CustomInput from './custom-input';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing(1),
  },
  primary: {
    color: '#fff'
  }
}));

const SelectLimit = props => {
  const { onSelect, limit, dark } = props;
  const classes = useStyles();
  const handleChange = event => {
      onSelect(event.target.value);
  };
  return (
    <form className={classes.root} autoComplete="off">
      <FormControl className={classes.margin}>
        <InputLabel htmlFor="limit-customized-select" className={dark ? classes.primary: ''}>Number per page</InputLabel>
        <Select
          value={limit}
          onChange={handleChange}
          input={<CustomInput name="limit" id="limit-customized-select" />}
        >
          <MenuItem value={10}>
            <em>None</em>
          </MenuItem>
          <MenuItem value={5}>5</MenuItem>
          <MenuItem value={10}>10</MenuItem>
          <MenuItem value={15}>15</MenuItem>
          <MenuItem value={20}>20</MenuItem>
          <MenuItem value={25}>25</MenuItem>
          <MenuItem value={30}>30</MenuItem>
        </Select>
      </FormControl>
    </form>
  );
}

export default SelectLimit;