import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router";
import { blue } from '@material-ui/core/colors';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import useSignupMutation from '../../graphql/mutations/auth/register';
import { PASSWORD_LENGTH, EMAIL_INVALID, EMAIL_EMPTY,USERNAME_LENGTH, USERNAME_EMPTY, PASSWORD_EMPTY, EMAIL_ALREADY_EXIST, CONFIRM_PASSWORD_NOT_MATCH } from '../../utils/constant';
import { capitalize } from '../../utils/utils';

const useStyles = makeStyles(theme => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    marginTop: theme.spacing(1),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    backgroundColor: blue[500],
    '&:hover': {
        backgroundColor: blue[700],
    },
  },
}));

const RegisterForm = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [username, setUsername] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const [errorMessages, setErrorMessages] = useState(null);

    const handleError = ({ graphQLErrors }) => {
      if (graphQLErrors) {
          const error = graphQLErrors[0].message;
          const errorMessage = {
            password: '',
            email: '',
            username: '',
            confirmPassword: ''

          }
          error.split(',').map(e => {
              if (e === "PASSWORD_EMPTY") {
                errorMessage.password = PASSWORD_EMPTY;
              } else if (e === "PASSWORD_LENGTH") {
                errorMessage.password = PASSWORD_LENGTH;
              }

              if (e === "EMAIL_EMPTY") {
                errorMessage.email = EMAIL_EMPTY;
              } else if (e === "EMAIL_INVALID") {
                errorMessage.email = EMAIL_INVALID;
              }

              if (e === "USERNAME_EMPTY") {
                errorMessage.username = USERNAME_EMPTY;
              } else if (e === "USERNAME_LENGTH") {
                errorMessage.username = USERNAME_LENGTH;
              }
          })
          if (error.startsWith("AuthenticationError")) {
            errorMessage.email = EMAIL_ALREADY_EXIST;
          }
          setErrorMessages(errorMessage);
        }
    }

    // const handleError = ({networkError, graphQLErrors, operation}) => {
    //   if (graphQLErrors) {
    //     console.log('graphQLErrors: ', graphQLErrors[0].message);
    //       const error = graphQLErrors[0].message;
    //       if (error === 'security_exception') {
    //         setErrorMessage('Incorrect email or password')
    //       }
    //       setErrorMessage(graphQLErrors[0].message === 'security_exception');
    //     }
    //     if (networkError) {
    //       setErrorMessage( `[Network error ${operation.operationName}]: ${networkError.message}`);
    //     }
    // }


    let [signup, { loading }] = useSignupMutation(handleError);

    let history = useHistory();
    const classes = useStyles();

    const submit = async (e) => {
        e.preventDefault();
        if (confirmPassword !== password) {
          setErrorMessages({confirmPassword: CONFIRM_PASSWORD_NOT_MATCH});
        }
        
        const result = await signup({ username, email, password });

        if (result) {
          setErrorMessages(null);
          history.push('/login');
        }
        return result;
    }

    const handleChange = event => {
        const { name, value } = event.target;
        if (name === 'username') setUsername(capitalize(value));
        if (name === 'email') setEmail(value);
        if (name === 'password') setPassword(value);
        if (name === 'confirmPassword') setConfirmPassword(value);
    }

    return (
        <form className={classes.form} onSubmit={submit} noValidate>
              <Box>
                <InputLabel className={classes.label}>Name</InputLabel>
                    <TextField
                        error={errorMessages && errorMessages.username ? true: false}
                        id="outlined-bare-username"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        name="username"
                        onChange={handleChange}
                        required
                        inputProps={{ 'aria-label': 'bare' }}
                        fullWidth
                        value={username}
                        helperText={ errorMessages && errorMessages.username }
                    />
                </Box>
                <Box mt={{ xs: 1, sm: 1, md: 2 }}>
                    <InputLabel className={classes.label}>Email</InputLabel>
                    <TextField
                        error={errorMessages && errorMessages.email ? true: false}
                        id="outlined-bare-email"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        name="email"
                        required
                        type="email"
                        onChange={handleChange}
                        inputProps={{ 'aria-label': 'bare' }}
                        fullWidth
                        value={email}
                        helperText={ errorMessages && errorMessages.email }
                    />
                </Box>
                <Box mt={{ xs: 1, sm: 1, md: 2 }}>
                    <InputLabel className={classes.label}>Password</InputLabel>
                    <TextField
                        error={errorMessages && errorMessages.password ? true: false}
                        id="outlined-bare-password"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        name="password"
                        required
                        type="password"
                        onChange={handleChange}
                        inputProps={{ 'aria-label': 'bare' }}
                        fullWidth
                        value={password}
                        helperText={ errorMessages && errorMessages.password }
                    />
                </Box>
                <Box mt={{ xs: 1, sm: 1, md: 2 }}>
                    <InputLabel className={classes.label}>Confirm Password</InputLabel>
                    <TextField
                        error={errorMessages && errorMessages.confirmPassword ? true: false}
                        id="outlined-bare-confirm"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        name="confirmPassword"
                        type="password"
                        onChange={handleChange}
                        inputProps={{ 'aria-label': 'bare' }}
                        fullWidth
                        value={confirmPassword}
                        helperText={ errorMessages && errorMessages.confirmPassword }
                    />
                </Box>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={loading}
          >
            Register
          </Button>
        </form>
  );
}

export default RegisterForm;