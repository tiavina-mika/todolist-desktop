import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router";
import { blue } from '@material-ui/core/colors';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import useEditProfilMutation from '../../graphql/mutations/auth/edit-profil';
import { EMAIL_INVALID, EMAIL_EMPTY,USERNAME_LENGTH, USERNAME_EMPTY } from '../../utils/constant';

const useStyles = makeStyles(theme => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    marginTop: theme.spacing(1),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    backgroundColor: blue[500],
    '&:hover': {
        backgroundColor: blue[700],
    },
  },
}));

const EditProfilForm = props => {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const { currentUser } = props;

    const [errorMessages, setErrorMessages] = useState(null);

    useEffect(() => {
        setUsername(currentUser.username);
        setEmail(currentUser.email);
    }, [currentUser.username, currentUser.email])

    const handleError = ({ graphQLErrors }) => {
      if (graphQLErrors) {
          const error = graphQLErrors[0].message;
          const errorMessage = {
            username: '',
            email: ''
          }
          error.split(',').map(e => {
              if (e === "USERNAME_EMPTY") {
                errorMessage.username = USERNAME_EMPTY;
              } else if (e === "USERNAME_LENGTH") {
                errorMessage.username = USERNAME_LENGTH;
              }
              if (e === "EMAIL_EMPTY") {
                errorMessage.email = EMAIL_EMPTY;
              } else if (e === "EMAIL_INVALID") {
                errorMessage.email = EMAIL_INVALID;
              }
          })
          setErrorMessages(errorMessage);
        }
    }
    let [editProfil, { loading }] = useEditProfilMutation(handleError);

    let history = useHistory();
    const classes = useStyles();

    const submit = async (e) => {
        e.preventDefault();
        const result = editProfil({ username, email });

        if (result) {
          history.push('/profil');
          setErrorMessages(null);
        }
        return result;
    }

    const handleChange = event => {
        const { name, value } = event.target;
        if (name === 'username') setUsername(value);
        if (name === 'email') setEmail(value);
    }

    return (
        <form className={classes.form} onSubmit={submit} noValidate>
                <Box mt={2}>
                    <InputLabel className={classes.label}>Username</InputLabel>
                    <TextField
                        error={errorMessages && errorMessages.username ? true: false}
                        id="outlined-bare-username"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        name="username"
                        onChange={handleChange}
                        inputProps={{ 'aria-label': 'bare' }}
                        fullWidth
                        value={username}
                        helperText={ errorMessages && errorMessages.username }
                    />
                </Box>
                <Box mt={2}>
                    <InputLabel className={classes.label}>Email</InputLabel>
                    <TextField
                        error={errorMessages && errorMessages.email ? true: false}
                        id="outlined-bare-confirm"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        name="email"
                        type="email"
                        onChange={handleChange}
                        inputProps={{ 'aria-label': 'bare' }}
                        fullWidth
                        value={email}
                        helperText={ errorMessages && errorMessages.email }
                    />
                </Box>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={loading}
          >
            Save
          </Button>
        </form>
  );
}

export default EditProfilForm;