import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import useEditTodoMutation from '../../graphql/mutations/todo/edit-todo';
import { blue } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
 
  },
  button: {
    color: theme.palette.getContrastText(blue[500]),
    backgroundColor: blue[500],
    '&:hover': {
      backgroundColor: blue[700],
    },
  },
  label: {
      marginLeft: theme.spacing(1),
      fontSize: 14
  }
}));

export default function FormDialog(props) {
  const [values, setValues] = useState({
      title: '',
      description: '',
      checked: false
  });
  const [checked, setChecked] = useState(false)

  let [ editTodo, loading ] = useEditTodoMutation(props.variables);

  const classes = useStyles();

  useEffect(() => {
    setValues({
      title: props.todo.title,
      description: props.todo.description,
      checked: props.todo.checked,
    })
    setChecked(props.todo.checked);
  }, [props.todo.title, props.todo.description, props.todo.checked])

  const handleChange = name => event => {
    setValues({
        ...values, 
        [name]: event.target.value 
    });
  };

  const handleCheck = e => {
    setChecked(e.target.checked)
  }

  const handleClose = () => {
    props.onClose();
  };

  const handleSubmit = () => {
    const { title, description } = values;
    editTodo({id: props.todo.id, title, description, checked})
    if (!loading) {
        setValues({title: '', description: ''});
        setChecked(false);
        props.onClose();
    }
  }

  return (
    <div>
      <Dialog open={props.open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Edit Todo</DialogTitle>
        <DialogContent>
            <Box>
                <InputLabel className={classes.label}>Title</InputLabel>
                    <TextField
                        id="outlined-bare"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        onChange={handleChange('title')}
                        inputProps={{ 'aria-label': 'bare' }}
                        fullWidth
                        value={values.title}
                    />
                </Box>
                <Box mt={2}>
                    <InputLabel className={classes.label}>Description</InputLabel>
                    <TextField
                        id="outlined-multiline-static"
                        multiline
                        rows="4"
                        className={classes.textField}
                        onChange={handleChange('description')}
                        margin="normal"
                        variant="outlined"
                        inputProps={{ 'aria-label': 'bare' }}
                        fullWidth
                        value={values.description}
                    />
                </Box>
                <Box mt={2}>
                    <Switch
                        checked={checked}
                        onChange={handleCheck}
                        value={checked}
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                    />
                </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary" className={classes.button}>
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
