import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Box from '@material-ui/core/Box';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import AlarmAddIcon from '@material-ui/icons/AlarmAdd';
import AlarmOnIcon from '@material-ui/icons/AlarmOn';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { blue } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
    label: {
        fontWeight: 600
    },
    text: {
        marginTop: -10
    },
    icon: {
        color: '#a5a5a5',
        marginRight: 5
    },
    date: {
        color: '#a5a5a5',
        fontSize: 13,
        marginTop: 1
    },
    dialogTitle: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    checkedIcon: {
        color: "#2c9101"
    },
    uncheckedIcon: {
        color: '#ff0000'
    },
    button: {
        color: theme.palette.getContrastText(blue[500]),
        backgroundColor: blue[500],
        '&:hover': {
          backgroundColor: blue[700],
        },
    }
}));

const DialogDetail = props => {
    const { todo } = props;

    const classes = useStyles();

    const handleClose = () => {
        props.onClose();
    };

    return (
            <Dialog open={props.open} onClose={handleClose} aria-labelledby="dialog-detail" maxWidth="lg">
                <DialogTitle id="dialog-detail">
                    <Box display="flex" justifyContent="space-between" alignItems="flex-start">
                        <Typography>Detail</Typography>
                        { todo.checked 
                            ?   <CheckCircleOutlineIcon className={classes.checkedIcon}/>
                            :   <HighlightOffIcon className={classes.uncheckedIcon}/>
                        }
                    </Box>
                </DialogTitle>
                <DialogContent>
                    <Box>
                        <Typography variant="subtitle1" gutterBottom className={classes.label}>Title:</Typography>
                        <Typography variant="subtitle1" gutterBottom className={classes.text}>{ todo.title }</Typography>
                    </Box>
                    <Box>
                        <Typography variant="subtitle1" gutterBottom className={classes.label}>Description:</Typography>
                        <Typography variant="subtitle1" gutterBottom className={classes.text}>{ todo.description }</Typography>
                    </Box>
                    <Box display="flex" alignItems="flex-start" mt={1.5}>
                        <AlarmAddIcon className={classes.icon} fontSize="small" />
                        <Typography variant="subtitle1" gutterBottom className={classes.date}>{ todo.createdAt.toLocaleString('fr-FR')}</Typography>
                    </Box>
                    { todo.updatedAt &&
                        <Box display="flex" alignItems="flex-start" >
                            <AlarmOnIcon className={classes.icon} fontSize="small" />
                            <Typography variant="subtitle1" gutterBottom className={classes.date}>{ todo.updatedAt.toLocaleString('fr-FR')}</Typography>
                        </Box>
                    }

                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary" className={classes.button}>
                    Close
                </Button>
                </DialogActions>
            </Dialog>
    );
}

export default DialogDetail;

