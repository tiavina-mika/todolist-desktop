import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import CssBaseline from '@material-ui/core/CssBaseline';
import Header from './header/header';
import useDesingQuery from '../graphql/queries/design';

const useStyles = makeStyles({
    main: {
        height: '100vh'
    },
});

const MainLayout = props => {
    const classes = useStyles();
    const { children } = props;
    const [ design ] = useDesingQuery();

    return <Box className={classes.main} bgcolor={design && design.mode === "dark" ? design.secondaryColor : '#f2f2f2'}>
                <CssBaseline />
                <Header />
                { children }
            </Box>
}

export default MainLayout;
