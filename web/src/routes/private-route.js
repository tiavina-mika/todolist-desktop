import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import MainLayout from '../components/main-layout';

const PrivateRoute = ({ component: Component,  ...rest }) => (
    <Route {...rest} render={props => {
            if (localStorage.getItem('token')) {
                return (
                    <MainLayout>
                        <Component {...props}/>
                    </MainLayout>
                )
            }
            return <Redirect to={{ pathname: '/login' }}/>;
        }   
    }/>
);

export default PrivateRoute;