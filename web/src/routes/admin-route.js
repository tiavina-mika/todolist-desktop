import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import MainLayout from '../components/main-layout';

const AdminRoute = ({ component: Component,  ...rest }) => (
    <Route {...rest} render={props => localStorage.getItem('token') && localStorage.getItem('user') === 'true'
        ?   <MainLayout>
                <Component {...props}/>
            </MainLayout>
        : <Redirect to={{ pathname: '/' }}/>
    }/>
);

export default AdminRoute;