import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PublicRoute = ({ component: Component,  ...rest }) => (
    <Route {...rest} render={props => {
            if (localStorage.getItem('token')) {
                return <Redirect to={{ pathname: '/' }}/>
            }
            return <Component {...props}/>;
        }   
    }/>
);

export default PublicRoute;