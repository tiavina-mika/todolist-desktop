import React from 'react';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import Login from './components/pages/login';
import Home from './components/pages/home';
import Logout from './components/pages/logout';
import Register from './components/pages/register';
import Users from './components/pages/users';
import Profil from './components/pages/profil';
import ChangePassword from './components/pages/change-password';
import CheckEmail from './components/pages/check-email';
import EditProfil from './components/pages/edit-profil';
import PrivateRoute from './routes/private-route';
import AdminRoute from './routes/admin-route';
import PublicRoute from './routes/public-route';

const App = () => (
    <Router>
        <Switch>
          <PublicRoute path="/login" component={Login}/>
          <PublicRoute path="/register" component={Register}/>
          <PublicRoute path="/check-email" component={CheckEmail}/>
          <PublicRoute path="/new-password/:id" component={ChangePassword}/>
          <Route path="/logout" component={Logout}/>
          <PrivateRoute path="/" exact component={Home}/>
          <PrivateRoute path="/profil" component={Profil}/>
          <PrivateRoute path="/change-password" component={ChangePassword}/>
          <PrivateRoute path="/edit-profil" component={EditProfil}/>
          <AdminRoute path="/users" component={Users}/>
        </Switch>    
    </Router>
);

export default App;