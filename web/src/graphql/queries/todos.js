import { gql } from 'apollo-boost';
import { useQuery } from '@apollo/react-hooks';

export const GET_TODOS = gql`
query GetTodos($sort: SortTodo, $limit: Int, $page: Int, $checked: Boolean){

    todos (sort: $sort, limit: $limit, page: $page, checked: $checked) {
        todos {
            id,
            title,
            description,
            checked
            createdAt,
            updatedAt      
        },
        currentPage,
        pages,
        total,
        __typename
        }
    }
`;

const useTodosQuery = (sort, limit, page, checked) => useQuery(GET_TODOS, { variables: { sort, limit, page, checked } });

export default useTodosQuery; 