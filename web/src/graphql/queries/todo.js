import { gql } from 'apollo-boost';
import { useQuery } from '@apollo/react-hooks';

export const GET_TODO = gql`
    query GetTodo($id: ID!){
        todo (id: $id) {
            id,
            title,
            description,
            checked    
        }
    }
`;

const useTodoQuery = () => useQuery(GET_TODO);

export default useTodoQuery; 