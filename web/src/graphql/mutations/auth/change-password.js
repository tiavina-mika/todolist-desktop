import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import {GET_PROFIL as profilQuery} from './profil';

export const CHANGE_PASSWORD  = gql`
    mutation SetChangePassword ($password: String!) {
        changePassword (password: $password) {
            updatedAt
        }
    }
`;

export default onError => {
    let [mutate, { loading, data, error }] = useMutation(CHANGE_PASSWORD, { onError });

    return [({ password }) => {
        return mutate({
            variables: { password },
            update: (cache, { data: { changePassword } }) => {     
                try {        
                    const data = cache.readQuery({ query: profilQuery });  
                    cache.writeQuery({ 
                        query: profilQuery, 
                        data: { 
                            profil: {
                                updatedAt: changePassword.updatedAt,
                                username: data.profil.username,
                                email: data.profil.email,
                                __typename: 'User'
                            }
                        } 
                    });    
                }      
                catch(error) { 
                    console.error(error);      
                }
            },
        });
    }, { loading, data, error } ];
};