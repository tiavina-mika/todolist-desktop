import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import {GET_PROFIL as profilQuery} from './profil';

export const EDIT_PROFIL  = gql`
    mutation SetEditProfil ($username: String, $email: String) {
        editProfil (username: $username, email: $email) {
            username,
            email,
            updatedAt
        }
    }
`;

export default onError => {
    let [mutate, { loading, data, error }] = useMutation(EDIT_PROFIL, { onError });

    return [({ username, email }) => {
        return mutate({
            variables: { username, email },
            update: (cache, { data: { editProfil } }) => {     
                try {        
                    cache.writeQuery({ 
                        query: profilQuery, 
                        data: { 
                            profil: editProfil
                        } 
                    });    
                }      
                catch(error) { 
                    console.error(error);      
                }
            },
        });
    }, { loading, data, error } ];
};