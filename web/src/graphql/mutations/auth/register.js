import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';

export const mutation = gql`
    mutation Signup($username: String!, $email: String!, $password: String!) {
        signup (username: $username, email: $email, password: $password) {
            id
            username
            email
            createdAt
        }
    }
`;

export default onError => {
    let [mutate, { loading, data, error }] = useMutation(mutation, { onError });

    return [({ username, email, password }) => {
        return mutate({
            variables: { username, email, password }
        });
    }, { loading, data, error } ];
};