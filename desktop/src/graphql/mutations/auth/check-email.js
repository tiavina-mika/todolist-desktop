import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';

export const CHECK_EMAIL  = gql`
    mutation SetCheckEmail ($email: String!) {
        checkEmail (email: $email) {
            email
            id
        }
    }
`;

export default onError => {
    let [mutate, { loading, data, error }] = useMutation(CHECK_EMAIL, { onError });

    return [({ email }) => {
        return mutate({
            variables: { email }
        });
    }, { loading, data, error } ];
};