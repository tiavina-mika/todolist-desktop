import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';

export const FORGOTTEN_EMAIL  = gql`
    mutation SetForgottenPassword ($id: ID!, $password: String!) {
        forgottenPassword (id: $id, password: $password) {
            email
        }
    }
`;

export default onError => {
    let [mutate, { loading, data, error }] = useMutation(FORGOTTEN_EMAIL, { onError });

    return [({ id, password }) => {
        return mutate({
            variables: { id, password }
        });
    }, { loading, data, error } ];
};