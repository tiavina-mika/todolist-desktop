import { gql } from 'apollo-boost';
import { useQuery } from '@apollo/react-hooks';

export const GET_PROFIL = gql`
    query Profil {
        profil {
            username,
            email,
            createdAt,
            updatedAt
        }
    }
`;

export default () => useQuery(GET_PROFIL);