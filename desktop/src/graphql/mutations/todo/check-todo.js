import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import {GET_TODOS as todosQuery} from '../../queries/todos'

export const mutation = gql`
    mutation CheckTodo($id: ID!, $checked: Boolean) {
        checkTodo (id: $id, checked: $checked)
    }
`;

export default ({ sort, limit, page, check }) => {
    let [mutate] = useMutation(mutation);

    return ({ id, checked }) => {
        return mutate({
            variables: { id, checked },
            update: cache => {     
                try {        
                    const data = cache.readQuery({ query: todosQuery, variables: {sort, limit, page, checked: check} });

                    cache.writeQuery({ 
                        query: todosQuery, 
                        variables: { sort, limit, page, checked: check },
                        data: { 
                            todos: {  
                                todos: data.todos.todos.map(todo => {
                                    if (todo.id === id) {
                                        todo.checked = checked;
                                    }
                                    return todo;
                                }),
                                total: data.todos.total,
                                currentPage: data.todos.currentPage,
                                pages: data.todos.pages,
                                __typename: "PaginatedTodo"
                            },
                        } 
                    });    
                }      
                catch(error) { 
                    console.error(error);      
                }},
            });
    };
};