import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import {GET_TODOS as todosQuery} from '../../queries/todos'

export const mutation = gql`
    mutation EditTodo($id: ID!, $title: String!, $description: String, $checked: Boolean) {
        editTodo (id: $id, title: $title, description: $description, checked: $checked) {
            id
            title
            description
            createdAt
            updatedAt
            checked
        }
    }
`;

export default ({ sort, limit, page, check }) => {
    let [mutate, { loading }] = useMutation(mutation);
 
    return [({ id, title, description, checked }) => {
        return mutate({
            variables: { id, title, description, checked },
            update: (cache, { data: { editTodo }} )=> {     
                try {        
                    const data = cache.readQuery({ query: todosQuery, variables: { sort, limit, page, checked: check } });   
                    const currentTodo = data.todos.todos.findIndex(todo => todo.id === editTodo.id)
                    cache.writeQuery({ 
                        query: todosQuery, 
                        variables: { sort, limit, page, checked: check },
                        data: { 
                            todos: {
                                todos: data.todos.todos.map(todo => {
                                    if (todo.id === currentTodo.id) {
                                        todo = currentTodo;
                                    }
                                    return;
                                }),
                                total: data.todos.total,
                                currentPage: data.todos.currentPage,
                                pages: data.todos.pages,
                                __typename: "PaginatedTodo"
                            }
                        } 
                    });    
                }      
                catch(error) { 
                    console.error(error);      
                }
            },
        });
    }, loading ];
};