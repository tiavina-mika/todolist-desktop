import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import {GET_TODOS as todosQuery} from '../../queries/todos'

export const mutation = gql`
    mutation DeleteTodos($checked: Boolean) {
        deleteTodos (checked: $checked)
    }
`;

export default ({ sort, limit, page, check }) => {
    let [mutate] = useMutation(mutation);

    return ({ checked }) => {
        return mutate({
            variables: { checked },
            update: cache => {     
                try {        
                    const data = cache.readQuery({ query: todosQuery, variables: {sort, limit, page, checked: check} });
                    const filteredData = data.todos.todos.filter(todo => todo.checked !== checked);
                    const newTodos = typeof(checked) === 'boolean' ? filteredData : [];
                    const total = typeof(checked) === 'boolean' ? filteredData.length: 0;

                    cache.writeQuery({ 
                        query: todosQuery, 
                        variables: { sort, limit, page, checked: check },
                        data: { 
                            todos: {  
                                todos: [...newTodos],
                                total: typeof(checked) === 'boolean' ? filteredData.length: 0,
                                currentPage: data.todos.currentPage,
                                pages: total === 0 ? 0 : data.todos.pages,
                                __typename: "PaginatedTodo"
                            },
                        } 
                    });    
                }      
                catch(error) { 
                    console.error(error);      
                }},
            });
    };
};