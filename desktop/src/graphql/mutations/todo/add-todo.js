import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import {GET_TODOS as todosQuery} from '../../queries/todos';

export const mutation = gql`
    mutation AddTodo($title: String!, $description: String, $checked: Boolean) {
        addTodo (title: $title, description: $description, checked: $checked) {
            id
            title
            description
            createdAt
            updatedAt
            checked
            user {
                id
                username
                email
                role
            }
        }
    }
`;

export default ({ sort, limit, page, check }, onError) => {
    let [mutate, { data, loading }] = useMutation(mutation, { onError });

    return [({ title, description, checked }) => {
        return mutate({
            variables: { title, description, checked },
            update: (cache, { data: { addTodo } }) => {     
                try {        
                    const data = cache.readQuery({ query: todosQuery, variables: {sort, limit, page, checked: check} });       
                    const newTodos = [addTodo, ...data.todos.todos ];
                    cache.writeQuery({ 
                        query: todosQuery, 
                        variables: { sort, limit, page, checked: check },
                        data: { 
                            todos: {  
                                todos: newTodos,
                                total: newTodos.length,
                                currentPage: data.todos.currentPage,
                                pages: data.todos.pages,
                                __typename: "PaginatedTodo"
                            }
                        } 
                    });    
                }      
                catch(error) { 
                    console.error(error);      
                }},
            });
    }, { loading, data } ]
    ;
};