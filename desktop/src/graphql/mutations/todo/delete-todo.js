import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import {GET_TODOS as todosQuery} from '../../queries/todos'

export const mutation = gql`
    mutation DeleteTodo($id: ID!) {
        deleteTodo (id: $id)
    }
`;

export default ({ sort, limit, page, check }) => {
    let [mutate] = useMutation(mutation);

    return id => {
        return mutate({
            variables: { id },
            update: cache => {     
                try {        
                    const data = cache.readQuery({ query: todosQuery, variables: {sort, limit, page, checked: check} });       
                    const newTodos = data.todos.todos.filter(todo => todo.id !== id);

                    cache.writeQuery({ 
                        query: todosQuery, 
                        variables: { sort, limit, page, checked: check },
                        data: { 
                            todos: {  
                                todos: [...newTodos],
                                total: newTodos.length,
                                currentPage: data.todos.currentPage,
                                pages: data.todos.pages,
                                __typename: "PaginatedTodo"
                            }
                        } 
                    });    
                }      
                catch(error) { 
                    console.error(error);      
                }},
            });
    };
};