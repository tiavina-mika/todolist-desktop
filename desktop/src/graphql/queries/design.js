import { gql } from 'apollo-boost';
import useAppState from "@helloncanella/useappstate";

export const GET_DESIGN = gql`
    query design {
        design @client
    }
`;

export default () => useAppState({query: GET_DESIGN});