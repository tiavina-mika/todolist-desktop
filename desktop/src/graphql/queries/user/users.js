import { gql } from 'apollo-boost';
import { useQuery } from '@apollo/react-hooks';

export const GET_USERS = gql`
query GetUsers($sort: SortUser, $limit: Int, $page: Int, $confirmed: Boolean){

    users (sort: $sort, limit: $limit, page: $page, confirmed: $confirmed) {
        users {
            id,
            username,
            email,
            confirmed,
            createdAt,
            updatedAt      
        },
        currentPage,
        pages,
        total,
        __typename
        }
    }
`;

const useUsersQuery = (sort, limit, page, confirmed) => useQuery(GET_USERS, { variables: { sort, limit, page, confirmed } });

export default useUsersQuery; 