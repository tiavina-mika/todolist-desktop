import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import CustomInput from './custom-input';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing(1),
  },
  primary: {
    color: '#fff'
  }
}));

const SelectStatus = props => {
  const { onSelect, check, dark } = props;
  const classes = useStyles();
  const handleChange = event => {
      onSelect(event.target.value);
  };
  return (
    <form className={classes.root} autoComplete="off">
      <FormControl className={classes.margin}>
        <InputLabel htmlFor="check-customized-select" className={dark ? classes.primary: ''}>Status</InputLabel>
        <Select
          value={check}
          onChange={handleChange}
          input={<CustomInput name="check" id="check-customized-select" />}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={true}>Done</MenuItem>
          <MenuItem value={false}>Not done</MenuItem>
          <MenuItem value={'none'}>All</MenuItem>
        </Select>
      </FormControl>
    </form>
  );
}

export default SelectStatus;