import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import CustomInput from './custom-input';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing(1),
  },
  primary: {
    color: '#fff'
  }
}));

const CustomizedSelect = props => {
  const { onSelect, sort, type, dark } = props;
  const classes = useStyles();
  const handleChange = event => {
      onSelect(event.target.value);
  };

  let valueType = type && type === "user" ? "USERNAME": "TITLE";
  let valueName = type && type === "user" ? "Username": "Title";

  return (
    <form className={classes.root} autoComplete="off">
      <FormControl className={classes.margin}>
        <InputLabel htmlFor="sort-customized-select" className={dark ? classes.primary: ''}>Sort By</InputLabel>
        <Select
          value={sort}
          onChange={handleChange}
          input={<CustomInput name="sort" id="sort-customized-select" />}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value="CREATEDAT">Date création</MenuItem>
          <MenuItem value="UPDATEDAT">Date modification</MenuItem>
          <MenuItem value={valueType}>{valueName}</MenuItem>
        </Select>
      </FormControl>
    </form>
  );
}

export default CustomizedSelect;