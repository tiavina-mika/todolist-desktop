import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router";
import { blue } from '@material-ui/core/colors';
import useLoginMutation from '../../graphql/mutations/auth/login';
import { PASSWORD_LENGTH, EMAIL_INVALID, EMAIL_EMPTY, PASSWORD_EMPTY, EMAIL_NOT_FOUND } from '../../utils/constant';

const useStyles = makeStyles(theme => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    marginTop: theme.spacing(1),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    backgroundColor: blue[500],
    '&:hover': {
        backgroundColor: blue[700],
    },
  },
}));

const LoginForm = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessages, setErrorMessages] = useState({
      password: '',
      email: ''
    });

    let history = useHistory();
    const classes = useStyles();

    const handleError = ({ graphQLErrors }) => {
      if (graphQLErrors) {
          const error = graphQLErrors[0].message;
          const errorMessage = {
            password: '',
            email: '',
          }

          error.split(',').map(e => {
              if (e === "PASSWORD_EMPTY") {
                errorMessage.password = PASSWORD_EMPTY;
              } else if (e === "PASSWORD_LENGTH") {
                errorMessage.password = PASSWORD_LENGTH;
              }

              if (e === "EMAIL_EMPTY") {
                errorMessage.email = EMAIL_EMPTY;
              } else if (e === "EMAIL_INVALID") {
                errorMessage.email = EMAIL_INVALID;
              }
              return e;
          })
          if (error.startsWith("AuthenticationError")) {
            errorMessage.email = EMAIL_NOT_FOUND;
          }
          setErrorMessages(errorMessage);
        }
    }

    let  [ login ] = useLoginMutation(handleError);

    const submit = async e => {
        e.preventDefault();
        try {
          const result = await login({ email, password })
          if (result) {
              localStorage.setItem("token", result.data.login.token);
              localStorage.setItem("user", result.data.login.isAdmin);
              history.push("/");
          }
        } catch (e) {
            console.log('error: ', e);
        }
    }

    const handleChange = event => {
        const { name, value } = event.target;
        if (name === 'email') setEmail(value);
        if (name === 'password') setPassword(value);
    }

    return (
        <form className={classes.form} onSubmit={submit}>
          <TextField
            error={ errorMessages && errorMessages.email ? true: false}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            value={email}
            onChange={handleChange}
            helperText={ errorMessages && errorMessages.email }
          />
          <TextField
            error={ errorMessages && errorMessages.password ? true: false}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={password}
            onChange={handleChange}
            helperText={ errorMessages && errorMessages.password }
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
        </form>
  );
}

export default LoginForm;