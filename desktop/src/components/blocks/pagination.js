import React from 'react';
import Pagination from "material-ui-flat-pagination";
import { makeStyles } from '@material-ui/core/styles';
import { grey, blue } from '@material-ui/core/colors';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: 20
    },
    textPrimary: {
        color: blue[700],
        fontSize: 16
    },
    textPrimaryDark: {
        color: '#fff',
        fontSize: 16
    },
    rootCurrentLight: {
        color: theme.palette.getContrastText(grey[500]),
        fontSize: 16,
        '&:hover': {
            backgroundColor: 'transparent'
        }
    },
    rootCurrentDark: {
        color: '#cecece',
        fontSize: 16,
        '&:hover': {
            backgroundColor: 'transparent'
        }
    },
    primary: {
        color: '#fff'
      },
      secondary: {
        color: '#cecece'
      }
}));
const CustomPagination = props => {
    const { currentPage, total, limit, onChangePage, dark } = props;
    const classes = useStyles();

    const handleClick = offset => {
        onChangePage(offset)
    }
 
    return (
        <Pagination
            limit={limit}
            offset={currentPage}
            total={total}
            onClick={(e, offset) => handleClick(offset)}
            nextPageLabel={<ArrowRightIcon fontSize="small"/>}
            previousPageLabel={<ArrowLeftIcon fontSize="small"/>}
            classes={{
                textPrimary: dark ? classes.textPrimaryDark: classes.textPrimary,
                rootCurrent: dark ? classes.rootCurrentDark: classes.rootCurrentLight ,
                root: classes.root,
            }}
        />
    );
}

export default CustomPagination;