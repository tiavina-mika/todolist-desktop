import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { blue } from '@material-ui/core/colors';

const ColorButton = withStyles(theme => ({
    root: {
        color: theme.palette.getContrastText(blue[500]),
        backgroundColor: blue[500],
        '&:hover': {
            backgroundColor: blue[700],
        },
    },
}))(Button);

const CustomButton = props => {
    const { type, text, onClick, fullWidth } = props;
    return (
          <ColorButton variant="contained" color="primary" type={type} onClick={onClick} fullWidth={fullWidth}>
              { text }
          </ColorButton>
    );
}

export default CustomButton;