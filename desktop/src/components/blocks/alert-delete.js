import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useDeleteSelected from '../../graphql/mutations/todo/delete-selected'

const AlertDelete = props => {
  const [open, setOpen] = useState(false);
  
  const { variables: { sort, limit, page, checked } } = props;

  let deleteTodos = useDeleteSelected({ sort, limit, page, check: checked });

  useEffect(() => {
      setOpen(props.open);
  }, [props.open])

  const handleClose = () => {
    setOpen(false);
    props.onClose();
  };

  const handleSubmit = async () => {
    const result = await deleteTodos(props.filter);
    if (result.data.deleteTodos) {
        handleClose();
    }
  }

  let text = "Are you sure to delete all task?";

  if (props.filter === true) {
      text = ""
      text = "Are you sure to delete not all done task?"
  } else {
    text = ""
    text = "Are you sure to delete all not done task?"
  }

  return (
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Delete</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
                {text}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary" autoFocus>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
  );
}

export default AlertDelete;