import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { blue } from '@material-ui/core/colors';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import { useHistory } from 'react-router-dom';
import EmojiObjectsIcon from '@material-ui/icons/EmojiObjects';
import useDesingQuery from '../../graphql/queries/design';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles(theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: blue[500],
  },
  title: {
    flexGrow: 1,
    color: '#fff',
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  menuIcon: {
      color: '#000'
  },
  menuItemIcon: {
    marginRight: 5
  },
  menu: {
    paddingLeft: 18,
    paddingRight: 8,
  },
  link: {
    textDecoration: 'none',
    color: '#fff'
  }
}));

const Header = props => {
  const classes = useStyles();
  const history = useHistory();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const [ design, setDesign ] = useDesingQuery();

  const handleDesign = () => {
    design && design.mode === "dark" 
      ? setDesign({
        mode: 'light'
      })
      : setDesign({
        primary: "#fff",
        secondaryColor: '#303030',
        primaryColor: '#424242',
        boxShadow: '1px 1px 1px #828282, -1px -1px 1px #828282',
        mode: 'dark'
      })
  }

  const handleMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    history.push('/logout');
    handleClose();
  }
  const handleProfil = () => {
    history.push('/profil');
    handleClose();
  }
  const handleUser = () => {
    history.push('/users');
  }

  return (
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
              <Link to="/" className={classes.link}>
                  Demo Todo
              </Link>
          </Typography>
          <div>
            { localStorage.getItem('user') === 'true' &&
            <Tooltip title="User list">
              <IconButton
                color="inherit"
                onClick={handleUser}
              >
                  <AssignmentIndIcon  fontSize="large"/>
              </IconButton>
            </Tooltip>
            }
            <Tooltip title="Change theme">
                <IconButton
                  color="inherit"
                  onClick={handleDesign}
                >
                    <EmojiObjectsIcon  fontSize="large"/>
                </IconButton>
            </Tooltip>
            <Tooltip title="Profil">
                <IconButton
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  onClick={handleMenu}
                  color="inherit"
                >
                    <AccountCircle fontSize="large"/>
                </IconButton>
              </Tooltip>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={open}
                onClose={handleClose}
                className={classes.menu}
              >
                <MenuItem onClick={handleProfil}>
                    <PermIdentityIcon className={classes.menuItemIcon}/>
                    <Typography>Profile</Typography> 
                </MenuItem>
                <MenuItem onClick={handleLogout}>
                    <PowerSettingsNewIcon className={classes.menuItemIcon}/>
                    <Typography>Logout</Typography> 
                </MenuItem>
              </Menu>
            </div>
        </Toolbar>
      </AppBar>
  );
}

export default Header;