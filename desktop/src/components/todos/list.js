import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';
import EditIcon from '@material-ui/icons/Edit';
import ControlPointIcon from '@material-ui/icons/ControlPoint';

import useDeleteTodoMutation from '../../graphql/mutations/todo/delete-todo';
import useCheckTodoMutation from '../../graphql/mutations/todo/check-todo';
import DialogEditForm from './dialog-edit-form';
import DialogDetail from './dialog-detail';
import { blue } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  paper: {
    padding: theme.spacing(3),
    minWidth: 500
  },
  paperForm: {
        display: 'flex',
        alignItems: 'flex-end',
        marginBottom: theme.spacing(2)
  },
  listItem: {
    borderBottom: '1px solid rgba(0, 0, 0, 0.2)'
  },
  listItemDark: {
    borderBottom: '1px solid #cecece'
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
  checkIcon: {
    color: blue[500],
    '&:hover': {
      color: blue[700],
    },
  },
  primary: {
    color: '#fff'
  },
  secondary: {
    color: '#cecece'
  }
}));


const TodoList = props => {
    const { variables: { sort, limit, page, checked }, todos, dark } = props;

    const classes = useStyles();
    const [selected, setSelected] = useState({todo: null, open: false, type: null})
    let deleteTodo = useDeleteTodoMutation({ sort, limit, page, check: checked });
    let checkTodo = useCheckTodoMutation({ sort, limit, page, check: checked });

    const handleOpenEdit = (todo) => {
      setSelected({todo, open: !selected.open, type: 'edit'})
    }
    const handleOpenDetail = (todo) => {
      setSelected({todo, open: !selected.open, type: 'detail'})
    }
    
    const handleClose = () => {
      setSelected({todo: null, open: false, type: null})
    }

    const filterTodo = selected.todo && todos.filter(t => t.id === selected.todo.id);
    const filterTodoId = filterTodo && filterTodo[0].id;
    
    return (
      <>
        { todos.map(todo => (
            <ListItem className={dark ? classes.listItemDark: classes.listItem} key={todo.id}>
                <ListItemAvatar>
                    <Avatar style={{backgroundColor: 'transparent'}}>
                      { todo.checked
                          ? <RadioButtonCheckedIcon color="primary" onClick={() => checkTodo({id: todo.id, checked: !todo.checked})} className={classes.checkIcon}/>
                          : <RadioButtonUncheckedIcon color="primary" onClick={() => checkTodo({id: todo.id, checked: !todo.checked})} className={classes.checkIcon}/>
                      }
                        
                    </Avatar>
                </ListItemAvatar>
                <ListItemText
                    primary={todo.title}
                    secondary={todo.description}
                    classes={dark? { primary: classes.primary, secondary: classes.secondary } : {}}
                />
                <ListItemSecondaryAction>
                    <IconButton
                      edge="end"
                      aria-label="detail"
                      className={dark? classes.secondary: ''}
                      onClick={()=> handleOpenDetail(todo)}
                      >
                      <ControlPointIcon />
                    </IconButton>
                    <IconButton
                      edge="end"
                      aria-label="edit"
                      className={dark? classes.secondary: ''}
                      onClick={()=> handleOpenEdit(todo)}>
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      edge="end"
                      aria-label="delete"
                      className={dark? classes.secondary: ''}
                      onClick={() => deleteTodo(todo.id)}>
                      <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        ))}
          { selected.type === "edit" && selected.todo && selected.todo.id === filterTodoId
              && <DialogEditForm 
                    open={selected.open}
                    onClose={handleClose}
                    todo={selected.todo}
                    variables={{ sort, limit, page, check: checked }}
                  />
          }
            { selected.type === "detail" && selected.todo && selected.todo.id === filterTodoId
              && <DialogDetail
                    open={selected.open}
                    onClose={handleClose}
                    todo={selected.todo}
                  />
          }
          
      </>
  );
}

export default TodoList;