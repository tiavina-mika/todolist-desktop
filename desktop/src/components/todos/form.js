import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '../blocks/button';
import useAddTodoMutation from '../../graphql/mutations/todo/add-todo';
import { capitalize } from '../../utils/utils';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    [theme.breakpoints.up('md')]: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),     
    },
  },
  label: {
      [theme.breakpoints.up('md')]: {
            marginLeft: theme.spacing(1),        
      },
      fontSize: 14
  },
  box: {
    [theme.breakpoints.down('md')]: {
      width: 400
    },
  },
}));

const TodoForm = props => {
    const { variables: { sort, limit, page, checked }, design } = props;

    const classes = useStyles();
    const [values, setValues] = useState({
        title: '',
        description: '',
    });
    const [errorMessages, setErrorMessages] = useState(null);

    const handleError = ({ graphQLErrors }) => {
        if (graphQLErrors) {
            const error = graphQLErrors[0].message;
            const errorMessage = {
              title: ''
            }
            error.split(',').map(e => {
                if (e === "TITLE_EMPTY") {
                  errorMessage.title = 'Title is required';
                } else if (e === "TITLE_LENGTH") {
                  errorMessage.title = 'Title must be between 5 and 50 caracters';
                }
            })
            setErrorMessages(errorMessage);
          }
      }
    let [ addTodo ] = useAddTodoMutation({ sort, limit, page, check: checked }, handleError);

    
    const handleChange = name => event => {
        setValues({
            ...values, 
            [name]: event.target.value 
        });
    };  

    const handleSubmit = async event => {
        event.preventDefault();
        const { title, description } = values;
        const result = await addTodo({ title: capitalize(title), description: capitalize(description), checked: false });

        if (result) setValues({title: '', description: ''})
        return result;
    };

    return (
        <form className={classes.container} autoComplete="off" onSubmit={handleSubmit}>
            <Box display="flex" flexDirection="column">
                <Box className={classes.box}>
                    <InputLabel
                        style={design ? { color: design.primary}: {}}
                        className={classes.label}
                    >
                            Title
                    </InputLabel>
                    <TextField
                        error={errorMessages && errorMessages.title ? true: false}
                        id="outlined-bare"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        onChange={handleChange('title')}
                        inputProps={{ 'aria-label': 'bare' }}
                        fullWidth
                        value={values.title}
                        helperText={ errorMessages && errorMessages.title }
                        style={design ? { backgroundColor: design.secondaryColor, boxShadow: design.boxShadow}: {}}
                    />
                </Box>
                <Box mt={2}>
                    <InputLabel
                        style={design ? { color: design.primary}: {}}
                        className={classes.label}
                    >
                        Description
                    </InputLabel>
                    <TextField
                        id="outlined-multiline-static"
                        multiline
                        rows="4"
                        className={classes.textField}
                        onChange={handleChange('description')}
                        margin="normal"
                        variant="outlined"
                        inputProps={{ 'aria-label': 'bare' }}
                        fullWidth
                        value={values.description}
                        style={design ? { backgroundColor: design.secondaryColor, boxShadow: design.boxShadow}: {}}
                    />
                </Box>
                <Box pl={1} mt={1.5}>
                    <Button text="Add" type="submit" fullWidth/>
                </Box>
            </Box>
        </form>
  );
}

export default TodoForm;