import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import useTodosQuery from '../../graphql/queries/todos';

import Pagination from '../blocks/pagination';
import DialogDelete from '../blocks/alert-delete';
import SelectSort from '../forms/select-sort';
import SelectStatus from '../forms/select-status';
import SelectLimit from '../forms/select-limit';
import SelectFilterDelete from '../forms/select-delete-status';
import TodoList from '../todos/list';
import TodoForm from '../todos/form';
import useDesingQuery from '../../graphql/queries/design';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  paper: {
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(3),
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
  },
  paperCenter: {
    [theme.breakpoints.up('md')]: {
      minWidth: 500,
    },
    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(3),
      marginTop: theme.spacing(2),
    },
  },
  paperRight: {
    [theme.breakpoints.up('md')]: {
      minWidth: 300,
      height: 370,
    },
    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(3),
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(5),
    },
  },
  paperLeft: {
    height: 360,
    [theme.breakpoints.up('md')]: {
      minWidth: 300,
    },
    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(3),
      marginRight: theme.spacing(2),
      minWidth: 400,
    },
  },
  paperForm: {
        display: 'flex',
        alignItems: 'flex-end',
        marginBottom: theme.spacing(2)
  },
  listItem: {
    borderBottom: '1px solid rgba(0, 0, 0, 0.2)'
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
}));

const Home = () => {
    const classes = useStyles();
    const [page, setPage] = useState(0);
    const [sort, setSort] = useState('CREATEDAT');
    const [check, setCheck] = useState('none');
    const [deleteFilter, setDeleteFilter] = useState(false);
    const [limit, setLimit] = useState(10);
    const [currentTodo, setCurrentTodo] = useState(null);
    const { loading, data } = useTodosQuery(sort, limit, page, typeof(check) === 'boolean' ? check: null);
    const [ design ] = useDesingQuery();

    useEffect(() => {
        data && setPage(data.todos.currentPage);
    }, [data])

    const handleChangePage = page => {
        setPage(page)
    }

    const handleSelectSort = sort => {
        setSort(sort);
    }
    const handleSelectCheck = check => {
      setCheck(check);
    }
    const handleSelectDeleteFilter = status => {
      setDeleteFilter(status);
    }

    const handleLimit = limit => {
        setLimit(parseInt(limit));
    }

    const handleCurrentTodo = todo => {
        setCurrentTodo(todo);
    }

    const handleCloseAlertDelete = () => {
      setDeleteFilter(false);
    }

    if (loading) return <Box display="flex" justifyContent="center"><CircularProgress/></Box>;

    const styleDarkPaper = design ? { backgroundColor: design.primaryColor, boxShadow: design.boxShadow }: {};
    const darkMode = design && design.mode === "dark";

    return (
        <div className={classes.root}>
            <Typography variant="h6" className={classes.title}>
                Your TodoList
            </Typography>
            <Box display="flex"flexDirection={{ xs: "column", sm: "column", md: "column", lg:"row" }}>
                <Paper className={clsx(classes.paper, classes.paperLeft)} style={styleDarkPaper}>
                    <TodoForm
                        variables={{sort, limit, page, checked: typeof(check) === 'boolean' ? check: null}}
                        currentTodo={currentTodo}
                        design={design}
                    />
                </Paper>
                <Paper className={clsx(classes.paper, classes.paperCenter)} style={styleDarkPaper}>
                  { data && data.todos.todos.length > 0
                    ? <List>
                        <TodoList
                            todos={data.todos.todos}
                            variables={{sort, limit, page, checked: typeof(check) === 'boolean' ? check: null}}
                            onEdit={handleCurrentTodo}
                            dark={darkMode}
                        />
                        { data.todos.pages < data.todos.total
                          && <Pagination
                                currentPage={page}
                                total={data.todos.total}
                                onChangePage={handleChangePage}
                                limit={limit}
                                dark={darkMode}
                              />
                        }
                    </List>
                    : <Box display="flex" justifyContent="center" alignItems="center" height="30vh">
                        <Typography variant="h6" style={darkMode ? { color: "#fff"}: {}}>No Task Found</Typography>
                    </Box>          
                  }
                </Paper>
                <DialogDelete
                  open={deleteFilter ? true: false}
                  onClose={handleCloseAlertDelete}
                  filter={deleteFilter}
                  variables={{sort, limit, page, checked: typeof(check) === 'boolean' ? check: null}}
                />
                <Paper className={clsx(classes.paper, classes.paperRight)} style={styleDarkPaper}>
                    <Box>
                        <SelectSort onSelect={handleSelectSort} sort={sort} type="todo" dark={darkMode}/>
                        <SelectStatus onSelect={handleSelectCheck} check={check} dark={darkMode}/>
                        { data && data.todos.todos.length > 1 &&
                          <>
                            <SelectLimit onSelect={handleLimit} limit={limit} dark={darkMode}/>
                            <SelectFilterDelete onSelect={handleSelectDeleteFilter} deleteFilter={deleteFilter} dark={darkMode}/>
                          </>
                        }
                    </Box>
                </Paper>
            </Box>
    </div>
  );
}

export default Home;