import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import RegisterForm from '../forms/register-form';
import { Link } from 'react-router-dom';
import { blue } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    [theme.breakpoints.down('md')]: {
      marginTop: 0,
      paddingBottom: theme.spacing(8)
    },
    [theme.breakpoints.up('md')]: {
      marginTop: theme.spacing(8),
    },
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  error: {
    color: '#9e1c1c',
    marginTop: theme.spacing(3)
  },
  link: {
    textDecoration: 'none',
    color: blue[500],
    '&:hover': {
        color: blue[700],
    },
  }
}));

const Register = () => {
  const classes = useStyles();

  // const handleError = ({networkError, graphQLErrors, operation}) => {
  //   if (graphQLErrors) {
  //       const error = graphQLErrors[0].message;
  //       if (error === 'security_exception') {
  //         setErrorMessage('Incorrect email or password')
  //       }
  //       setErrorMessage(graphQLErrors[0].message === 'security_exception');
  //     }
  //     if (networkError) {
  //       setErrorMessage( `[Network error ${operation.operationName}]: ${networkError.message}`);
  //     }
  // }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
            Register
        </Typography>
        <RegisterForm/>
      </div>
      <Box mt={2} display="flex" justifyContent="flex-start">
          <Typography>Have already an account? <Link to="/login" className={classes.link}> Signin</Link></Typography>
        </Box>
    </Container>
  );
}

export default Register;