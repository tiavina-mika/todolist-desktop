import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import CheckEmailForm from '../forms/check-email-form';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  container: {
      paddingTop: theme.spacing(15)
  },
  main: {
      backgroundColor: theme.palette.common.white,
  },
}));

const CheckEmail = () => {
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs" className={classes.container}>
      <CssBaseline />
      <Box display="flex" flexDirection="column" alignItems="center" pl={5} pt={3}  pr={5} pb={5} className={classes.main}>
        <Typography component="h1" variant="h5">
          Enter your email
        </Typography>
        <CheckEmailForm />
      </Box>
    </Container>
  );
}

export default CheckEmail;