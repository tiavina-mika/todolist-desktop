import { useApolloClient } from "@apollo/react-hooks";
import { useHistory } from "react-router-dom";

const  Logout = () => {
    const history = useHistory();
    const client = useApolloClient();
    
    client.resetStore();
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    history.push('/login');

    return null;
}

export default Logout;