import React from 'react';
import { Link } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import FaceIcon from '@material-ui/icons/Face';
import MoodIcon from '@material-ui/icons/Mood';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import useProfilQuery from '../../graphql/mutations/auth/profil';
import { blue } from '@material-ui/core/colors';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import AlarmAddIcon from '@material-ui/icons/AlarmAdd';
import useDesingQuery from '../../graphql/queries/design';

const useStyles = makeStyles(theme => ({
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  paper: {
    [theme.breakpoints.up('md')]: {
      width: 450,
      height: '25vh',
    },
    [theme.breakpoints.down('md')]: {
      height: '65vh',
      width: 400
    },
    marginTop: theme.spacing(3),
    borderRadius: 8,
    display: 'flex',
    justifyContent: 'center'
  },
  link: {
    color: blue[500],
    '&:hover': {
        color: blue[700],
    },
  },
  icon: {
      marginRight: 8
  },
  text: {
      fontSize: 18
  },
}));

const Profil = () => {
  const classes = useStyles();
  const { loading, data } = useProfilQuery();
  const [ design ] = useDesingQuery();

  if (loading) return <Box display="flex" justifyContent="center"><CircularProgress/></Box>;

  return (
      <Box display="flex" flexDirection="column" alignItems="center" mt={8}>
        <Avatar className={classes.avatar}>
          <FaceIcon />
        </Avatar>
        <Paper className={classes.paper} elevation={2} style={{ backgroundColor: design ? design.primaryColor : ''}}>
          <div>
            <Box display="flex" alignItems="flex-end" justifyContent="flex-start" mt={5}>
                <MoodIcon
                    className={classes.icon}
                    style={{ color: design && design.mode === "dark" ? design.primary: ''}}
                />
                <Typography
                  variant="subtitle2"
                  className={classes.text}
                  style={{ color: design && design.mode === "dark" ? design.primary: ''}}>                  
                    {data.profil.username}
                </Typography>
            </Box>
            <Box display="flex" alignItems="flex-end" justifyContent="flex-start" mt={5}>
                <ContactMailIcon
                  className={classes.icon}
                  style={{ color: design && design.mode === "dark" ? design.primary: ''}}
                />
                <Typography
                  variant="subtitle2"
                  className={classes.text}
                  style={{ color: design && design.mode === "dark" ? design.primary: ''}}>
                    {data.profil.email}
                </Typography>
            </Box>
            <Box display="flex" alignItems="flex-end" justifyContent="flex-start" mt={5}>
                    <AlarmAddIcon
                        className={classes.icon}
                        style={{ color: design && design.mode === "dark" ? design.primary: ''}}
                    />
                    <Typography
                      variant="subtitle1"
                      className={classes.text}
                      style={{ color: design && design.mode === "dark" ? design.primary: ''}}>
                        { data.profil.createdAt.toLocaleString('fr-FR')}
                    </Typography>
            </Box>
          </div>
        </Paper>
        <Box display="flex" alignItems="flex-end" justifyContent="center" mt={5} >
              <Typography
                variant="subtitle1"
                className={classes.text}
                style={{ color: design && design.mode === "dark" ? design.primary: ''}}>
                  <Link
                      className={classes.link}
                      to="/change-password"
                      style={{ color: design && design.mode === "dark" ? design.primary: ''}}>
                         Change password
                  </Link>
              </Typography>
          </Box>
          <Box display="flex" alignItems="flex-end" justifyContent="center" mt={5} mb={5}>
              <Typography
                variant="subtitle1"
                className={classes.text}
                style={{ color: design && design.mode === "dark" ? design.primary: ''}}>
                  <Link
                      className={classes.link}
                      to="/edit-profil"
                      style={{ color: design && design.mode === "dark" ? design.primary: ''}}>
                         Edit profil
                  </Link>
              </Typography>
          </Box>
      </Box>
  )
}

export default Profil;