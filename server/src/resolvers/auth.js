import UserModel from '../models/user'
import bcrypt from 'bcryptjs'
import { validateLogin, validateSignup, validateChangePassword, validateEmail } from '../utils/validations'
import security from '../utils/security'
import { validateUser } from '../utils/validations'

export default {
    Mutation: {
        login: async (_, args ) => {
            validateLogin(args)
            const { email, password } = args
            const user = await UserModel.findOne({ email })
            if (!user) throw new AuthenticationError('User not found')
            let isAdmin = false
            const isMatch = await bcrypt.compare(password, user.password)

            if (user && !isMatch) throw new AuthenticationError('Not not found')
            if (user.role === "ADMIN") {
                isAdmin = true
            }
            return {
                email,
                token: security.encode(email, password),
                isAdmin
            }
        },
        signup: async (_, args) => {
            validateSignup(args)
            const { username, email, password } = args
            const user = await UserModel.findOne({ email })
            if (user) throw new AuthenticationError()
            const newUser = new UserModel({ username, email, password })
            bcrypt.genSalt(10, async (_, salt) => {
                bcrypt.hash(password, salt, async (_, hash) => {
                    newUser.password = hash
                    if (newUser.email === process.env.ADMIN_EMAIL) {
                        newUser.role = 'ADMIN'
                    }
            
                    const result = await newUser.save()
                    return result
                })
            })

            if (!newUser) throw new AuthenticationError()
            return newUser
        },
        changePassword: async (_, args, { user, currentUser }) => {
            validateChangePassword(args)
            const { password } = args;
            if (!user) throw new AuthenticationError()
            if (!currentUser) throw new AuthenticationError()
            bcrypt.genSalt(10, async (_, salt) => {
                bcrypt.hash(password, salt, async (_, hash) => {
                    currentUser.password = hash
                    currentUser.updatedAt = Date.now()
            
                    const result = await currentUser.save()
                    return result
                })
            })
            return currentUser
        },
        editProfil: async (_, args, { user, currentUser } ) => {
            const { username, email } = args
            if (!user) throw new AuthenticationError()
            if (!currentUser) throw new AuthenticationError()

            validateUser(args)
            currentUser.set({ username, email })
            currentUser.updatedAt = Date.now()
            const editedUser = await currentUser.save()

            return editedUser
        },
        checkEmail: async (_, args ) => {
            validateEmail(args)
            const { email } = args
            const user = await UserModel.findOne({ email })
            if (!user) throw new AuthenticationError('User not found')

            return { email: user.email, id: user.id }
        },
        forgottenPassword: async (_, args) => {
            validateChangePassword(args)
            const { id, password } = args;
            const user = await UserModel.findById(id).exec()

            if (!user) throw new AuthenticationError()

            bcrypt.genSalt(10, async (_, salt) => {
                bcrypt.hash(password, salt, async (_, hash) => {
                    user.password = hash
                    user.updatedAt = Date.now()
            
                    const result = await user.save()
                    return result
                })
            })
            return user
        },
    }
}