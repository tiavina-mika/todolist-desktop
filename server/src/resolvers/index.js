import todoResolver from './todo'
import authResolver from './auth'
import userResolver from './user'
import dateSCalarResolver from './dateScalar'

export default [
    todoResolver,
    userResolver,
    authResolver,
    dateSCalarResolver
]