import UserModel from '../models/user'

export default {
    Query: {
        users: async (_, { sort , limit, page, confirmed }, { user, isAdmin }) => {
            const query = {}
            let querySort = 'createdAt'

            if (!user) throw new AuthenticationError()
            if (!isAdmin) throw new AuthenticationError()

            if (typeof(confirmed) === 'boolean') {
                query.confirmed = confirmed
            }
            switch (sort) {
              case 'CREATEDAT':
                  querySort = '-createdAt'
                  break
              case 'UPDATEDAT':
                  querySort = '-updatedAt'
                  break
              case 'USER':
                  querySort = 'username'
                  break
            }
    
            const count = await UserModel.countDocuments(query)
            const resPerPage = parseInt(limit) || count
            const pageNumber = parseInt(page) || 1
            
            const users = await UserModel.find(query)    
                .skip((resPerPage * pageNumber) - resPerPage)
                .limit(resPerPage)
                .sort(querySort)
    
            return {
                users: users, 
                'currentPage': pageNumber, 
                'pages' : Math.ceil(count / resPerPage), 
                total: count
            }
        },
        profil: async (_, __, { user, currentUser }) => {
            if (!user) throw new AuthenticationError()
            if (!currentUser) throw new AuthenticationError()
            return currentUser
        }
    },
    Mutation: {
        deleteUser: async (_, { id } , { user, isAdmin }) => {
            if (!user) throw new AuthenticationError()
            if (!isAdmin) throw new AuthenticationError()

            const result = await UserModel.findById(id).exec()
            const removedUser = await result.remove()
            if (!removedUser) {
              return false
            }
            return true
        },
        deleteUsers: async (_ ,__ , { isAdmin }) => {
            if (!isAdmin) throw new AuthenticationError()
            const removedCheckedUsers = await UserModel.deleteMany()
            if (!removedCheckedUsers) {
              return false
            }
            return true
        },
        confirmUser: async (_, { id, confirmed }, { user }) => {
            if (!user) throw new AuthenticationError()
            const confirmedTodo = await UserModel.findByIdAndUpdate(id, { confirmed, updatedAt: Date.now() })
            if (confirmedTodo) {
                return true
            }
            return false
      },
    }
}