import TodoModel from '../models/todo'
import { validateTodo } from '../utils/validations'
import { UserInputError } from 'apollo-server'

export default {
    Query: {
        todos: async (_, { sort , limit, page, checked }, { user, currentUser }) => {
            const query = {user: currentUser}
            let querySort = 'createdAt'
            if (!user) throw new AuthenticationError()
    
            if (typeof(checked) === 'boolean') {
                query.checked = checked
            }
            switch (sort) {
              case 'CREATEDAT':
                  querySort = '-createdAt'
                  break
              case 'UPDATEDAT':
                  querySort = '-updatedAt'
                  break
              case 'TITLE':
                  querySort = 'title'
                  break
            }
    
            const count = await TodoModel.countDocuments(query)
            const resPerPage = parseInt(limit) || count
            const pageNumber = parseInt(page) || 1
            
            const todos = await TodoModel.find(query)    
                .skip((resPerPage * pageNumber) - resPerPage)
                .limit(resPerPage)
                .sort(querySort)
    
            return {
                todos: todos, 
                'currentPage': pageNumber, 
                'pages' : Math.ceil(count / resPerPage), 
                total: count
            }
        },
        todo: async (_, { id }, { user }) => {
            if (!user) throw new AuthenticationError()
            const todo = await TodoModel.findById(id).exec()
            return todo
        },
    },
    Mutation: {
        addTodo: async (_, args, { user, currentUser } ) => {
            if (!user) throw new AuthenticationError()
            const { title, description, checked } = args
            validateTodo(args)
            const todo = new TodoModel({title, description, checked, user: currentUser});

            try {
                await todo.save()
            } catch (e) {
                throw new UserInputError(error.message, {
                    invalidArgs: args,
                })
            }
            return todo;
        },
        editTodo: async (_, args, { user } ) => {
            if (!user) throw new AuthenticationError()
            const { id, title, description, checked } = args
            validateTodo(args)
            const todo = await TodoModel.findById(id).exec()
            todo.set({ title, description, checked })
            todo.updatedAt = Date.now()
            
            try {
                await todo.save()
            } catch (e) {
                throw new UserInputError(error.message, {
                    invalidArgs: args,
                })
            }
            return todo
        },
        deleteTodo: async (_, { id }, { user }) => {
            if (!user) throw new AuthenticationError()
            const todo = await TodoModel.findById(id).exec()
            const removedTodo = await todo.remove()
            if (!removedTodo) {
              return false
            }
            return true
        },
        deleteTodos: async (_, { checked }, { user }) => {
            if (!user) throw new AuthenticationError()
            const removedCheckedTodos = await TodoModel.deleteMany(typeof(checked) === 'boolean' && { checked })
            if (!removedCheckedTodos) {
              return false
            }
            return true
        },
        checkTodo: async (_, { id, checked }, { user }) => {
            if (!user) throw new AuthenticationError()
            const checkedTodo = await TodoModel.findByIdAndUpdate(id, { checked, updatedAt: Date.now() })
            if (checkedTodo) {
                return true
            }
            return false
      },
    }
}