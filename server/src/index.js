import { ApolloServer } from 'apollo-server'
import mongoose from 'mongoose'
import resolvers from './resolvers/index'
import typeDefs from './schemas/typeDefs'
import context from './context/context'
import 'dotenv/config'

const server = new ApolloServer({ typeDefs, resolvers, context })

mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})
mongoose.set('useCreateIndex', true)

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`)
})