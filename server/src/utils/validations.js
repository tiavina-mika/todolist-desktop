import { UserInputError } from 'apollo-server'
import validator from 'validator'

export const validateTodo = (values) => {
    const errorMessage = []
    if(validator.isEmpty(values.title)) {
        errorMessage.push("TITLE_EMPTY")
    } else if(!validator.isLength(values.title, {min: 5, max: 50})) {
        errorMessage.push("TITLE_LENGTH")
    }

    if (errorMessage.length > 0) {
        throw new UserInputError(errorMessage, { invalidArgs: "errors-input-todo"})
    }
}

export const validateUser = values => {
    const errorMessage = []

    if(validator.isEmpty(values.username)) {
        errorMessage.push("USERNAME_EMPTY")
    } else if(!validator.isLength(values.username, {min: 5, max: 50})) {
        errorMessage.push("USERNAME_LENGTH")
    }
    if(validator.isEmpty(values.email)) {
        errorMessage.push("EMAIL_EMPTY")
    } else if(!validator.isEmail(values.email)) {
        errorMessage.push("EMAIL_INVALID")
    }

    if (errorMessage.length > 0) {
        throw new UserInputError(errorMessage, { invalidArgs: "errors-input-user" })    
    }
}

export const validateSignup = values => {
    const errorMessage = []

    if(validator.isEmpty(values.username)) {
        errorMessage.push("USERNAME_EMPTY")
    } else if(!validator.isLength(values.username, {min: 5, max: 50})) {
        errorMessage.push("USERNAME_LENGTH")
    }
    if(validator.isEmpty(values.password)) {
        errorMessage.push("PASSWORD_EMPTY")
    } else if(!validator.isLength(values.password, {min: 6, max: 30})) {
        errorMessage.push("PASSWORD_LENGTH")
    }
    if(validator.isEmpty(values.email)) {
        errorMessage.push("EMAIL_EMPTY")
    } else if(!validator.isEmail(values.email)) {
        errorMessage.push("EMAIL_INVALID")
    }

    if (errorMessage.length > 0) {
        throw new UserInputError(errorMessage, { invalidArgs: "errors-input-signup"})
    }
}

export const validateLogin = values => {
    const errorMessage = []

    if(validator.isEmpty(values.password)) {
        errorMessage.push("PASSWORD_EMPTY")
    } else if(!validator.isLength(values.password, {min: 6, max: 30})) {
        errorMessage.push("PASSWORD_LENGTH")
    }
    if(validator.isEmpty(values.email)) {
        errorMessage.push("EMAIL_EMPTY")
    } else if(!validator.isEmail(values.email)) {
        errorMessage.push("EMAIL_INVALID")
    }

    if (errorMessage.length > 0) {
        throw new UserInputError(errorMessage, { invalidArgs: "errors-input-signin"})
    }
}

export const validateChangePassword = values => {
    const errorMessage = passwordValidation(values.password)

    if (errorMessage) {
        throw new UserInputError(errorMessage, { invalidArgs })    
    }
}

const passwordValidation = password => {
    let errorMessage
    if(validator.isEmpty(password)) {
        errorMessage = "PASSWORD_EMPTY"
    } else if(!validator.isLength(password, {min: 6, max: 30})) {
        errorMessage = "PASSWORD_LENGTH"
    }
    return errorMessage;
}


export const validateEmail = values => {
    const errorMessage = []

    if(validator.isEmpty(values.email)) {
        errorMessage.push("EMAIL_EMPTY")
    } else if(!validator.isEmail(values.email)) {
        errorMessage.push("EMAIL_INVALID")
    }

    if (errorMessage.length > 0) {
        throw new UserInputError(errorMessage, { invalidArgs: "errors-input-signup"})
    }
}

