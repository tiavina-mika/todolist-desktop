import crypto from "crypto"
import jwt from "jwt-simple"

const algorithm = "aes-256-cbc"

const encrypt = text => {
  let cipher = crypto.createCipheriv(
    algorithm,
    process.env.CRYPTO_SECRET_KEY.slice(0, 32),
    process.env.CRYPTO_SECRET_IV.slice(0, 16)
  )
  let encrypted = cipher.update(text)
  encrypted = Buffer.concat([encrypted, cipher.final()])
  return encrypted.toString("hex")
}

const decrypt = text => {
  let encryptedText = Buffer.from(text, "hex")
  let decipher = crypto.createDecipheriv(
    algorithm,
    process.env.CRYPTO_SECRET_KEY.slice(0, 32),
    process.env.CRYPTO_SECRET_IV.slice(0, 16)
  )
  let decrypted = decipher.update(encryptedText)
  decrypted = Buffer.concat([decrypted, decipher.final()])
  return decrypted.toString()
}

const encode = (email, password) =>
  jwt.encode(
    { le: encrypt(email), ri: encrypt(password) },
    process.env.JWT_SECRET_KEY
  )

const decode = token => {
  const payload = jwt.decode(token, process.env.JWT_SECRET_KEY)
  return { email: decrypt(payload.le), password: decrypt(payload.ri) }
}

export default { encode, decode }
