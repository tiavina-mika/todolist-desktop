import security from "../utils/security"
import UserModel from "../models/user"

export default async ({ req }) => {
  const bearer = req.headers.authorization || ""
  if (bearer !== "" && bearer.toLowerCase().indexOf("bearer") === -1) {
    throw new Error("Invalid Authorization header")
  }
  let user = null
  let currentUser = null
  let isAdmin = false
  if (bearer !== "") {
    const token = bearer.split(" ")[1]
    user = security.decode(token)
    currentUser = await UserModel.findOne({ email: user.email })
    if (currentUser && currentUser.role === "ADMIN") isAdmin = true 
  }
  return { user, currentUser, isAdmin }
}
