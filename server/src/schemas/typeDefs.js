import { gql } from 'apollo-server'


export default gql`
    enum SortTodo {
      CREATEDAT
      UPDATEDAT
      TITLE
    }
    enum SortUser {
      CREATEDAT
      UPDATEDAT
      USERNAME
    }
    type Query {
        todos (sort: SortTodo, limit: Int, page: Int, checked: Boolean): PaginatedTodo,
        checkedTodos: [Todo],
        todo (id: ID!): Todo,

        users (sort: SortUser, limit: Int, page: Int, confirmed: Boolean): PaginatedUser,
        profil: User
    }
    type Mutation {
        addTodo (title: String!, description: String, checked: Boolean): Todo,
        editTodo (id: ID!, title: String!, description: String, checked: Boolean): Todo,
        deleteTodo (id: ID!): Boolean!,
        deleteTodos (checked: Boolean): Boolean!,
        checkTodo (id: ID!, checked: Boolean): Boolean!,

        login(email: String!, password: String!): AuthPayload,
        signup(username: String!, email: String!, password: String!): User,
        changePassword(password: String!): User,
        checkEmail(email: String!): EmailPayload,
        forgottenPassword(id: ID!,password: String!): User,

        editProfil (username: String, email: String): User,
        deleteUser (id: ID!): Boolean!,
        deleteUsers: Boolean!,
        confirmUser (id: ID!, confirmed: Boolean): Boolean!,
    }

    type AuthPayload {
        token: String!
        email: String!
        isAdmin: Boolean!
    }
    type EmailPayload {
        id: String!
        email: String!
    }

    scalar Date

    type Todo {
        id: ID!
        title: String!
        description: String
        checked: Boolean
        createdAt: Date
        updatedAt: Date
        user: User!
    }
    type User {
        id: ID!
        username: String!
        email: String!
        password: String!
        role: String
        createdAt: Date
        updatedAt: Date
        confirmed: Boolean
    }
    type PaginatedTodo {
      todos: [Todo]
      currentPage: Int
      pages: Int
      total: Int
    }
    type PaginatedUser {
      users: [User]
      currentPage: Int
      pages: Int
      total: Int
    }
  
`