import mongoose from 'mongoose'

const Schema = mongoose.Schema

const TodoSchema = new Schema(
    {
        title: {type: String, required: true, min: 5, max: 50},
        description: {type: String},
        checked: {type: Boolean, default: false},
        createdAt: {type: Date, default: Date.now},
        updatedAt: {type: Date},
        user: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    }
)

export default mongoose.model('Todo', TodoSchema)